# Setup the `Caravel` dependencies

`caravel-deps-setup` is a command-line utility written in `Rust` that takes care 
of the tedious job of installing all the dependencies of `Caravel` automatically.

The dependencies to be installed can be specified via an input file and the location of 
the directory where the dependencies are going to be installed can be provided
either on the command-line or via an environment variable.

In addition, a temporary directory (for the lack of a better name) for the downloaded tarballs, 
the extracted source files and git repositories 
is created. This directory will also contain a global log file `main_log.log` that summarizes the 
different steps in installing the dependencies. In addition, for each dependency, a log file is stored
that contains the output of the different command line tools that are being invoked in order to simplify
debugging in case something goes wrong.

By specifying the command line option `-s` a source file `source.sh` is created in the temporary
directory that can be sourced using the command

```sh
$ source source.sh
```

in order to setup the environment such that the `Caravel` build system can find the dependencies.


***

## Requirements

The utility performs the installation by downloading the source codes, un-compressing them
and invoking their build systems. For this reason it should be clear that all the tools necessary
to perform these actions needs to be installed and available in the __PATH__ environment variable.
The tools that are needed are the following:

- wget  
- make  
- ninja  
- meson  
- cmake  
- autoreconf  
- git  
- tar  

## Install

In order to install `caravel-deps-setup`, the `Rust` compiler needs to be available which can be obtained by following the instructions 
at [https://www.rust-lang.org/tools/install.](https://www.rust-lang.org/tools/install). Once the compiler is available, the binary can be 
compiled by running

```sh
$ cargo build --release
```

from the root directory of the repository.

## Usage

To get a general overview of the usage, query the command-line help

```
$ ./target/release/caravel-deps-setup -h
```

An example for the usage is the following:

```sh
& ./target/release/caravel-deps-setup /prefix/path --config config.toml --source-file --force -n 8 --temp caravel-deps-temp
```

- Here, the install *prefix* is specified as the first positional argument. It is required, unless an environment variable 
__CARAVEL\_DEPS\_PREFIX__ exists that contains the same information. 

- The configuration file is specified after the `--config/-c` flag.
It default to `config.toml` (the default file is distributed in this repository). If the default file is not present, the argument needs
to be provided. 

- The flag `--source-file` instructs the utility to generate a file at the end, that can be sourced to setup the environment
for the `Caravel` build system to find all dependencies. This flag is optional. 

- The number of cores to be used for the compilation of the
dependencies is specified by the flag `-n`. If not explicitly set, it defaults to using all available cores. Notice that the dependencies are 
compiled sequentially.

- Finally, the `--temp` flag stores the path to the directory where all the downloaded files will be stored together with the log files that
summarize the different steps. This defaults to `caravel-deps-temp` and the directory will be created if not present.

## Input 

The input file contains for each dependency the URL where the source can be downloaded or cloned as well as 
a flag about whether or not the dependency shall be installed. The standard file serving as an example can be found in 
[`config.toml`](config.toml).

## Documentation

The source code documentation can be generated and shown by running

```sh
$ cargo doc --no-deps --open
```

## License

`caravel-deps-setup` is distributed under the terms of the GNU General Public License (version 3) or later. See [LICENSE](LICENSE).