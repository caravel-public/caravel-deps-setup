//! Input parsing infrastructure.
//! 
//! This module derives the serialization attributes 
//! of the different input settings such that they can
//! be directly parsed from the toml configuration file 
//! based on `serde`.
//!
//! A convenience function is provided, `parse_input`, that 
//! takes as input the path to the configuration file and then
//! takes care of reading the content to a string, parsing 
//! the toml data and serializing it to the respective structs 
//! that can be handled in the code.

use crate::install::Dependencies;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing GMP.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not GMP shall be installed.
pub struct GMPSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing GSL.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not GSL shall be installed.
pub struct GSLSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing Eigen.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not Eigen shall be installed.
pub struct EigenSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing QD.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not QD shall be installed.
pub struct QDSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing the pentagon library published in [arXiv:1807.09812](https://arxiv.org/abs/1807.09812).
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not the pentagon library shall be installed.
pub struct PentagonSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing GiNaC.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not GiNaC shall be installed.
pub struct GiNaCSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing CLN.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not CLN shall be installed.
pub struct CLNSettings {
    pub url: String,
    pub install: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// Collect the settings for installing MPFR.
///
/// This includes a URL where the source code
/// can be downloaded from an a flag indicating
/// whether or not MPFR shall be installed.
pub struct MPFRSettings {
    pub url: String,
    pub install: bool,
}

/// Parse the toml configuration file and return the dependency object.
pub fn parse_input<P: AsRef<std::path::Path>>(path: P) -> Dependencies {
    // Read the configuration file
    let configuration = match std::fs::read_to_string(path) {
        Ok(file) => file,
        Err(e) => {
            log::error!("Unable to read configuration file. Got error: \n{}", e);
            std::process::exit(1);
        }
    };

    // Parse the configuration file
    let configuration: Dependencies = match toml::from_str(&configuration) {
        Ok(config) => config,
        Err(e) => {
            log::error!(
                "Unable to parse the configuration file to toml. Got error: \n{}",
                e
            );
            std::process::exit(1);
        }
    };

    configuration
}
