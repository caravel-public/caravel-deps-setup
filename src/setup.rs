//! This module contains the setup infrastructure.
//! 
//! This includes a setup function for the logger as well 
//! as the functions that take care of creating the required
//! directories (if they do not yet exist).

use simplelog::*;
use std::path;
use crate::error::{InstallError, InstallErrorStage};

/// Setup the logger.
///
/// The logger will log to the terminal as well as to the file given by `path`.
pub fn setup_logger<P: AsRef<path::Path>>(path: P) {
    let config = ConfigBuilder::new().set_time_to_local(true).build();
    CombinedLogger::init(vec![
        TermLogger::new(LevelFilter::Debug, config, TerminalMode::Mixed)
            .expect("Unable to configure logger for logging to terminal."),
        WriteLogger::new(
            LevelFilter::Info,
            Config::default(),
            std::fs::File::create(path).expect("Unable to configure logger for logging to file."),
        ),
    ])
    .expect("Unable to setup logger")
}

/// Create a directory at the location given by `dir`.
///
/// Depending on whether `force` is set, the function panics in case the directory
/// exists (`force`=false) or removes the existing directory and replaces it with
/// an empty one (`force`=true).
fn create_directory<P: AsRef<path::Path> + std::fmt::Debug>(dir: P, force: bool) {
    // If the directory does not exist, create it.
    if !dir.as_ref().exists() {
        std::fs::create_dir_all(dir.as_ref())
            .unwrap_or_else(|_| panic!("Unable to create directory {:?}.", &dir));
    }
    // If the directory exists, check whether it is empty.
    let is_empty = dir.as_ref().read_dir().unwrap().next().is_none();
    // If not empty and the force flag is not set, error out.
    if !is_empty && !force {
        panic!(
            "Directory {:?} exists and is not empty. Rerun with `--force` to overwrite.",
            &dir
        );
    }
    // If not empty and the force flag is set, remove the content.
    if !is_empty && force {
        std::fs::remove_dir_all(dir.as_ref())
            .unwrap_or_else(|_| panic!("Unable to remove directory {:?}.", &dir));
        std::fs::create_dir_all(dir.as_ref())
            .unwrap_or_else(|_| panic!("Unable to remove directory {:?}.", &dir));
    }
}

/// Setup the temporary directory.
///
/// This directory contains the tarballs and the extracted source directories as well
/// as clones of git repositories and log files reporting on the process of the installation
/// of the different dependencies.
///
/// Depending on whether `force` is set, the function panics in case the directory
/// located in the path `dir` exists (`force`=false) or removes the existing directory and replaces it with
/// an empty one (`force`=true).
pub fn setup_temp_dir<P: AsRef<path::Path> + std::fmt::Debug>(dir: P, force: bool) {
    create_directory(&dir, force);
    // Create the log directory
    let log_dir: path::PathBuf = dir.as_ref().join("logs");
    std::fs::create_dir(&log_dir).unwrap_or_else(|_| panic!("Unable to remove directory {:?}.", &log_dir));
    // Create the tarball directory
    let tar_dir: path::PathBuf = dir.as_ref().join("tarballs");
    std::fs::create_dir(&tar_dir).unwrap_or_else(|_| panic!("Unable to remove directory {:?}.", &tar_dir));
    // Create the source directory
    let src_dir: path::PathBuf = dir.as_ref().join("src");
    std::fs::create_dir(&src_dir).unwrap_or_else(|_| panic!("Unable to remove directory {:?}.", &src_dir));
}

/// Create the install directory if it does not already exists.
pub fn setup_install_dir<P: AsRef<path::Path> + std::fmt::Debug>(dir: P) {
    if !dir.as_ref().exists() {
        log::info!("Creating install directory{:?}", &dir);
        if let Err(e) = std::fs::create_dir_all(dir.as_ref()) {
            log::error!("Unable to create directory {:?}. Got error:\n{}", &dir, e);
            std::process::exit(1);
        }
    }
}


/// Check the prerequisites
pub fn check_prerequisites<P: AsRef<path::Path> + std::fmt::Debug>(prereq: &str, path: P) {
    log::info!("Checking prerequisite '{}'", prereq);

    // check wget
    let cmd_out = std::process::Command::new(prereq)
        .stdout(
            {
                let log_options = std::fs::OpenOptions::new()
                            .create(true)
                            .append(true)
                            .open(&path)
                            .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))));
                match log_options {
                    Ok(o) => o,
                    Err(e) => {
                        log::error!("--- Unable to access log file: {:?} ({})", path, e);
                        std::process::exit(1);
                    }
                }
            }
        )
        .stderr(
            {
                let log_options = std::fs::OpenOptions::new()
                            .create(true)
                            .append(true)
                            .open(&path)
                            .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))));
                match log_options {
                    Ok(o) => o,
                    Err(e) => {
                        log::error!("--- Unable to access log file: {:?} ({})", path, e);
                        std::process::exit(1);
                    }
                }
            }
        )
        .arg("--version")
        .output();

    match cmd_out {
        Ok(o) => {
            if !o.status.success() {
                let code = o.status.code().unwrap_or(-1);
                log::warn!("--- Got error code {} for prerequisite {}.", code, prereq);
                log::info!("--- Depending on your configuration, you might get problems later.")
            } else {
                log::info!("--- OK!")
            }
        }
        Err(e) => {
            log::warn!("--- Problem for prerequisite ({}).", e);
            log::info!("--- Depending on your configuration, you might get problems later.")
        }
    }


}
