//! # `caravel-setup-deps`
//!
//! `caravel-deps-setup` is a command-line utility written in `Rust` that takes care
//! of the tedious job of installing all the dependencies of `Caravel` automatically.
//!
//! The dependencies to be installed can be specified via an input file and the location of
//! the directory where the dependencies are going to be installed can be provided
//! either on the command-line or via an environment variable.
//!
//! In addition, a temporary directory (for the lack of a better name) for the downloaded tarballs,
//! the extracted source files and git repositories
//! is created. This directory will also contain a global log file `main_log.log` that summarizes the
//! different steps in installing the dependencies. In addition, for each dependency, a log file is stored
//! that contains the output of the different command line tools that are being invoked in order to simplify
//! debugging in case something goes wrong.
//!
//! By specifying the command line option `-s` a source file `source.sh` is created in the temporary
//! directory that can be sourced using the command
//!
//! ```sh
//! $ source source.sh
//! ```
//!
//! in order to setup the environment such that the `Caravel` build system can find the dependencies.
//!
//!
//! ***
//!
//! ## Requirements
//!
//! The utility performs the installation by downloading the source codes, un-compressing them
//! and invoking their build systems. For this reason it should be clear that all the tools necessary
//! to perform these actions needs to be installed and available in the __PATH__ environment variable.
//! The tools that are needed are the following:
//!
//! - wget  
//! - make  
//! - ninja  
//! - meson  
//! - cmake  
//! - autoreconf  
//! - git  
//! - tar  
//!
//! ## Install
//!
//! In order to install `caravel-deps-setup`, the `Rust` compiler needs to be available which can be obtained by following the instructions
//! at [https://www.rust-lang.org/tools/install.](https://www.rust-lang.org/tools/install). Once the compiler is available, the binary can be
//! compiled by running
//!
//! ```sh
//! $ cargo build --release
//! ```
//!
//! from the root directory of the repository.
//!
//! ## Usage
//!
//! To get a general overview of the usage, query the command-line help
//!
//! ```
//! $ ./target/release/caravel-deps-setup -h
//! ```
//!
//! An example for the usage is the following:
//!
//! ```sh
//! & ./target/release/caravel-deps-setup /prefix/path --config config.toml --source-file --force -n 8 --temp caravel-deps-temp
//! ```
//!
//! - Here, the install *prefix* is specified as the first positional argument. It is required, unless an environment variable
//! __CARAVEL\_DEPS\_PREFIX__ exists that contains the same information.
//!
//! - The configuration file is specified after the `--config/-c` flag.
//! It default to `config.toml` (the default file is distributed in this repository). If the default file is not present, the argument needs
//! to be provided.
//!
//! - The flag `--source-file` instructs the utility to generate a file at the end, that can be sourced to setup the environment
//! for the `Caravel` build system to find all dependencies. This flag is optional.
//!
//! - The number of cores to be used for the compilation of the
//! dependencies is specified by the flag `-n`. If not explicitly set, it defaults to using all available cores. Notice that the dependencies are
//! compiled sequentially.
//!
//! - Finally, the `--temp` flag stores the path to the directory where all the downloaded files will be stored together with the log files that
//! summarize the different steps. This defaults to `caravel-deps-temp` and the directory will be created if not present.
//!
//! ## Input
//!
//! The input file contains for each dependency the URL where the source can be downloaded or cloned as well as
//! a flag about whether or not the dependency shall be installed. The standard file serving as an example can be found at
//! [`config.toml`](https://gitlab.com/caravel-private/caravel-deps-setup/-/blob/master/config.toml):
//!

use std::io::Write;
use structopt::StructOpt;

mod error;
mod input;
mod install;
mod setup;

/// Install the depedencies of the `Caravel` library.
#[derive(Debug, StructOpt)]
#[structopt(author)]
pub struct Cli {
    /// Number of cores (default: all) for installing dependencies
    #[structopt(short = "n")]
    n_cores: Option<usize>,
    /// Install location of the `Caravel` dependencies
    #[structopt(parse(from_os_str), env = "CARAVEL_DEPS_PREFIX")]
    prefix: std::path::PathBuf,
    /// Temporary storage for the install files.
    #[structopt(
        short = "t",
        long = "temp",
        default_value = "caravel-deps-temp",
        parse(from_os_str)
    )]
    temp: std::path::PathBuf,
    /// The location of the input file
    #[structopt(
        short = "c",
        long = "config",
        default_value = "config.toml",
        parse(from_os_str)
    )]
    config_file: std::path::PathBuf,
    /// Generate a bash script that can be sourced to setup the environment
    #[structopt(short = "s", long = "source-file")]
    source_file: bool,
    /// Force clearing non-empty directories
    #[structopt(short = "f", long = "force")]
    force: bool,
}

/// The `caravel-deps-setup` application
///
/// It parses the command-line arguments, reads and parses the input file
/// to determine which dependencies to install and sets up the necessary
/// directories.
///
/// Once all this is done, it proceeds to installing the specified dependencies
/// reporting on the overall progress in the `main_log.log` file.
/// The output of the various programs invoked during the installation of the
/// different dependencies is piped into `dep_name.log` files.
///
/// At the end the utility provides information on how to setup the environment
/// such that the `Caravel` build system can find the previously installed
/// dependencies. The required command is logged to the terminal (and therefore)
/// also appears in the main log file and upon request (`--source-file` or `-s` flags)
/// a shell script is generated that can be sourced to setup the environment.
fn main() {
    // Parse the command line arguments.
    let args = Cli::from_args();

    // Setup the temporary directory for storing source files and logs
    setup::setup_temp_dir(&args.temp, args.force);

    // Setup the install directory
    setup::setup_install_dir(&args.prefix);

    // Setting up the logger
    setup::setup_logger(args.temp.join("logs").join("main_log.log"));

    // Checking for prerequisites: installed software mentioned in the documentation
    setup::check_prerequisites("wget", args.temp.join("logs").join("prereq_wget.log"));
    setup::check_prerequisites("make", args.temp.join("logs").join("prereq_make.log"));
    setup::check_prerequisites("ninja", args.temp.join("logs").join("prereq_ninja.log"));
    setup::check_prerequisites("meson", args.temp.join("logs").join("prereq_meson.log"));
    setup::check_prerequisites("cmake", args.temp.join("logs").join("prereq_cmake.log"));
    setup::check_prerequisites("autoreconf", args.temp.join("logs").join("prereq_autoreconf.log"));
    setup::check_prerequisites("git", args.temp.join("logs").join("prereq_git.log"));
    setup::check_prerequisites("tar", args.temp.join("logs").join("prereq_tar.log"));

    // Read the configuration file
    log::info!("Parsing configuration file.");
    let configuration = input::parse_input(&args.config_file);

    // Determine the number of CPUs to use
    let n_cores = match args.n_cores {
        None => num_cpus::get(),
        Some(n) => n,
    };
    log::info!("Using {} cores.", n_cores);

    log::info!("Starting the installation process");
    configuration.install(args.temp.as_path(), args.prefix.as_path(), n_cores);
    log::info!("Installing dependencies done.");

    // Give information on how Caravel can find the dependencies
    let pkg_config_path = std::fs::canonicalize(args.prefix.join("lib").join("pkgconfig"))
        .expect("Unable to canonicalize the pkg_config_path.");
    let pkg_config_path = pkg_config_path
        .to_str()
        .expect("Unable to convert the pkg_config_path to string.");
    log::info!("To make the libraries available to the Caravel build system, run:");
    log::info!(
        "export PKG_CONFIG_PATH={}:$PKG_CONFIG_PATH",
        &pkg_config_path
    );
    let ld_library_path = std::fs::canonicalize(args.prefix.join("lib"))
        .expect("Unable to canonicalize the first ld_library_path.");
    let ld_library_path = ld_library_path
        .to_str()
        .expect("Unable to convert the ld_library_path to string.");
    log::info!("To make the libraries available at runtime, run:");
    log::info!(
        "export LD_LIBRARY_PATH={}:$LD_LIBRARY_PATH",
        &ld_library_path
    );


    // Generate a source file
    if args.source_file {
        let file = std::fs::File::create(args.temp.join("source.sh"));
        log::info!("Generating file source file to setup the environment");
        match file {
            Ok(mut f) => {
                if f.write_all(format!("export PKG_CONFIG_PATH={}:$PKG_CONFIG_PATH\nexport LD_LIBRARY_PATH={}:$LD_LIBRARY_PATH", &pkg_config_path, &ld_library_path).as_bytes()).is_err() {
                    log::warn!("Unable to create source.sh file.")
                }
            },
            Err(_) => log::warn!("Could not generate source file to setup environment. Skipping this step. Issue the command mentioned above.")
        };
        log::info!("Done.");
        log::info!("Setup the environment by running the following command:");
        log::info!("$ source {:?}", args.temp.join("source.sh"));
    }
}
