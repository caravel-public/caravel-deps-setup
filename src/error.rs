//! This module defines a global error type to be used across the library.
//!
//! TODO: Maybe implement conversions from the common error types of the 
//! standard library to avoid proliferations of `map_err` calls.

/// Possible stages at which errors can occur in the build chain.
#[derive(Debug, Clone)]
pub enum InstallErrorStage {
    /// Error during the setup stage.
    // Setup(String),
    /// Error during the configuration step.
    Configuration(String),
    /// Error during the installation step.
    Install(String),
    /// Error during file access.
    FileAccess(String),
    /// Error occurring during a download.
    Download(String),
    /// Error occurring during (de-) compression.
    Compression(String),
    /// Error cloning repository.
    Clone(String),
}

/// An common error returned upon failure while building the `Caravel` dependencies.
#[derive(Debug, Clone)]
pub struct InstallError {
    stage: InstallErrorStage,
}

impl InstallError {
    /// Construct a new `InstallError` indicating at which stage something went wrong.
    ///
    /// # Example
    ///
    /// ```rust
    /// use crate::building::{InstallError, InstallErrorStage}
    /// let error = InstallError::new(InstallErrorStage::FileAccess("File not found"));
    /// assert_eq!(format!("{}", error), String::from("Error accessing file: File not found"));
    /// ```
    pub fn new(stage: InstallErrorStage) -> InstallError {
        InstallError { stage }
    }
}

impl std::fmt::Display for InstallError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.stage {
            InstallErrorStage::Configuration(msg) => writeln!(f, "Error in config stage: {}", msg),
            InstallErrorStage::Install(msg) => writeln!(f, "Error in install stage: {}", msg),
            InstallErrorStage::FileAccess(msg) => writeln!(f, "Error in file access: {}", msg),
            InstallErrorStage::Download(msg) => writeln!(f, "Error during download: {}", msg),
            InstallErrorStage::Clone(msg) => writeln!(f, "Error during repository clone: {}", msg),
            InstallErrorStage::Compression(msg) => {
                writeln!(f, "Error during (de-) compression: {}", msg)
            }
        }
    }
}
