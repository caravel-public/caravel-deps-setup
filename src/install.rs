//! Collection of functions that install the different `Caravel` dependencies.
//!
//! This module is the workhorse for the command-line utility given by this crate.
//! No two installation processes for dependencies are the same which is why this
//! module contains a lot of code. Wherever possible, I have tried to factor out
//! common operations into functions.

use serde::{Deserialize, Serialize};

use crate::error::{InstallError, InstallErrorStage};
use crate::input::{
    CLNSettings, EigenSettings, GMPSettings, GSLSettings, GiNaCSettings, MPFRSettings,
    PentagonSettings, QDSettings,
};

#[derive(Serialize, Deserialize, Debug)]
/// Collect information necessary to build `Caravel` dependencies.
///
/// This struct can be directly parsed from the toml input file using serde.
pub struct Dependencies {
    gmp: GMPSettings,
    eigen: EigenSettings,
    qd: QDSettings,
    pentagon: PentagonSettings,
    ginac: GiNaCSettings,
    cln: CLNSettings,
    mpfr: MPFRSettings,
    gsl: GSLSettings,
}

impl Dependencies {
    /// Install the `Caravel` dependencies as defined in the configuration file.
    pub fn install(&self, temp: &std::path::Path, prefix: &std::path::Path, n_cores: usize) {
        let prefix = std::fs::canonicalize(prefix).expect("Unable to canonicalize the prefix.");
        // Install GMP
        if self.gmp.install {
            log::info!("Installing GMP");
            match install_gmp(&self.gmp.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of GMP: {}", e),
            }
        }
        // Install GSL
        if self.gsl.install {
            log::info!("Installing GSL");
            match install_gsl(&self.gsl.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of GSL: {}", e),
            }
        }

        // Install MPFR
        if self.mpfr.install {
            log::info!("Installing MPFR");
            if !self.gmp.install {
                log::warn!("Installing MPFR without installing GMP.");
                log::warn!("--- This might work if GMP has been installed previously in the provided prefix.");
                log::warn!("--- If it fails, consider setting `install=true` in the `GMP` section of your input file.");
            }
            match install_mpfr(&self.mpfr.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of MPFR: {}", e),
            }
        }
        // Install QD
        if self.qd.install {
            log::info!("Installing QD");
            match install_qd(&self.qd.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of QD: {}", e),
            }
        }
        // Install CLN
        if self.cln.install {
            log::info!("Installing CLN");
            match install_cln(&self.cln.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of CLN: {}", e),
            }
        }
        // Install GiNaC
        if self.ginac.install {
            if !self.cln.install {
                log::warn!("Requested building GiNaC without requesting CLN.");
                log::warn!("--- This might work if CLN has been installed previously and is available in the $PKG_CONFIG_PATH$ environment variable.");
                log::warn!("--- If you encounter problems, try setting `install=true` in the `CLN` section of the input file");
            }
            log::info!("Installing GiNaC");
            match install_ginac(&self.ginac.url, temp, &prefix, n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of GiNaC: {}", e),
            }
        }
        // Install Eigen
        if self.eigen.install {
            log::info!("Installing Eigen");
            match install_eigen(&self.eigen.url, temp, prefix.as_path(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of Eigen: {}", e),
            }
        }
        // Install Pentagon library
        if self.pentagon.install {
            log::info!("Installing Pentagon library");
            if !self.gsl.install {
                log::warn!("Installing PentagonLibrary without installing GSL");
                log::warn!("--- This might work if GSL has been installed previously in the provided prefix.");
                log::warn!("--- If it fails, consider setting `install=true` in the `GSL` section of your input file.");
            }
            if !self.ginac.install {
                log::warn!("Installing PentagonLibrary without installing GiNaC.");
                log::warn!("--- This might work if GiNaC has been installed previously in the provided prefix.");
                log::warn!("--- If it fails, consider setting `install=true` in the `GiNaC` section of your input file.");
            }

            match install_pentagons(&self.pentagon.url, temp, prefix.as_ref(), n_cores) {
                Ok(_) => log::info!("Done"),
                Err(e) => log::error!("Error during installing of Pentagon library: {}", e),
            }
        }
    }
}

/// Download compressed archives.
///
/// The archives are downloaded from the `url` (by calling `wget`)
/// and stored in the location given by `target_dir`.
///
/// The output of `wget` is appended to the log file located at
/// `log`.
fn download_compressed(
    url: &str,
    target_dir: &std::path::Path,
    log: &std::path::Path,
    name: &str,
) -> Result<(), InstallError> {
    let output = std::process::Command::new("wget")
        .current_dir(target_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg(url)
        .arg("-O")
        .arg(name)
        .output();

    match output {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    log::error!("Problem downloading tarball. Got error code: {}", code);
                    return Err(InstallError::new(InstallErrorStage::Download(format!(
                        "Problem downloading tarball. Got error code: {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Download(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Extract a `.tar.gz` archive.
/// The archive located at `input` is extracted to `out` using `tar`
/// and the output of `tar` is appended to the log file located at
/// `log`.
fn uncompress_tar_gz<P: AsRef<std::path::Path>>(
    input: P,
    out: P,
    log: &P,
) -> Result<(), InstallError> {
    let output = std::process::Command::new("tar")
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-xf")
        .arg(&format!("{}", input.as_ref().display()))
        .arg("-C")
        .arg(&format!("{}", out.as_ref().display()))
        .output();

    match output {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Compression(format!(
                        "Problem during uncompression.. Got error code: {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Compression(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Extract a `.tar.bz` archive.
/// The archive located at `input` is extracted to `out` using `tar`
/// and the output of `tar` is appended to the log file located at
/// `log`.
fn uncompress_tar_bz<P: AsRef<std::path::Path>>(
    input: P,
    out: P,
    log: &P,
) -> Result<(), InstallError> {
    let output = std::process::Command::new("tar")
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(log)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("xvjf")
        .arg(&format!("{}", input.as_ref().display()))
        .arg("-C")
        .arg(&format!("{}", out.as_ref().display()))
        .output();

    match output {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Compression(format!(
                        "Got error code: {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Compression(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install GMP
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, GMP is configured and installed.
fn install_gmp(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("gmp.log");
    std::fs::File::create(&log_path).expect("Unable to create log-file for GMP.");
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.xz", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(
        url,
        temp.join("tarballs").as_path(),
        &log_path,
        "gmp.tar.xz",
    )?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_gz(
        temp.join("tarballs").join("gmp.tar.xz"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for gmp.".to_string(),
        ))
    })?;
    // Configuring
    log::info!("--- Configuring gmp");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("--prefix")
        .arg(
            prefix
                .to_str()
                .expect("Unable to convert prefix path to str."),
        )
        .arg("--enable-cxx")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing gmp");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install GSL
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, GSL is configured and installed.
fn install_gsl(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("gsl.log");
    std::fs::File::create(&log_path).expect("Unable to create log-file for GSL.");
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.gz", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(
        url,
        temp.join("tarballs").as_path(),
        &log_path,
        "gsl.tar.gz",
    )?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_gz(
        temp.join("tarballs").join("gsl.tar.gz"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for gsl.".to_string(),
        ))
    })?;
    // Configuring
    log::info!("--- Configuring gsl");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("--prefix")
        .arg(
            prefix
                .to_str()
                .expect("Unable to convert prefix path to str."),
        )
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing gsl");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install CLN
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, CLN is configured and installed.
fn install_cln(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("cln.log");
    std::fs::File::create(&log_path).expect("Unable to create log-file for CLN.");
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.bz2", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(url, &temp.join("tarballs"), &log_path, "cln.tar.bz2")?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_bz(
        temp.join("tarballs").join("cln.tar.bz2"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for CLN.".to_string(),
        ))
    })?;
    // Configuring
    log::info!("--- Configuring CLN");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("--prefix")
        .arg(
            prefix
                .to_str()
                .expect("Unable to convert prefix path to str."),
        )
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing CLN");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install GiNaC
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, GiNaC is configured and installed.
fn install_ginac(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("ginac.log");
    std::fs::File::create(&log_path)
        .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?;
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.bz2", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(url, &temp.join("tarballs"), &log_path, "ginac.tar.bz2")?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_bz(
        temp.join("tarballs").join("ginac.tar.bz2"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for GiNaC.".to_string(),
        ))
    })?;
    // Configuring
    log::info!("--- Configuring GiNaC");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .env(
            "PKG_CONFIG_PATH",
            prefix
                .join("lib")
                .join("pkgconfig")
                .to_str()
                .expect("Unable to generate pkg-config prefix path from prefix"),
        )
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("--prefix")
        .arg(
            prefix
                .to_str()
                .expect("Unable to convert prefix path to str."),
        )
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing GiNaC");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install QD
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, QD is configured and installed.
///
/// The `configure.sh` script distributed with QD is not the correct one which is
/// why `autoreconf -fi` is executed before the configuration step.
fn install_qd(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("qd.log");
    std::fs::File::create(&log_path)
        .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?;
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.gz", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(url, &temp.join("tarballs"), &log_path, "qd.tar.gz")?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_gz(
        temp.join("tarballs").join("qd.tar.gz"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for QD.".to_string(),
        ))
    })?;
    // Run autoreconf
    log::info!("--- Auto-reconfiguring QD");
    let autoreconf = std::process::Command::new("autoreconf")
        .current_dir(&build_dir.join(".."))
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-fi")
        .output();

    match autoreconf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    // Configuring
    log::info!("--- Configuring QD");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("--prefix")
        .arg(
            prefix
                .to_str()
                .expect("Unable to convert prefix path to str."),
        )
        .arg("--enable-shared=yes")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing QD");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))))
                } else {
                    Ok(())
                }
            } else {
                Ok(())
            }
        }
        Err(e) => Err(InstallError::new(InstallErrorStage::Install(format!(
            "{}",
            e
        )))),
    }
}

/// Install MPFR
///
/// The tarball is downloaded into the directory `temp/tarballs` and subsquently
/// extracted to `temp/src`.
///
/// Once this is done, MPFR is configured and installed.
fn install_mpfr(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("mpfr.log");
    std::fs::File::create(&log_path)
        .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?;
    let src_name = url
        .to_string()
        .split('/')
        .last()
        .unwrap()
        .replace(".tar.gz", "");
    // Downloading the tarball
    log::info!("--- Downloading tarball");
    download_compressed(url, &temp.join("tarballs"), &log_path, "mpfr.tar.gz")?;
    // Uncompress the tarball
    log::info!("--- Uncompressig tarball");
    uncompress_tar_gz(
        temp.join("tarballs").join("mpfr.tar.gz"),
        temp.join("src"),
        &log_path,
    )?;
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join(src_name).join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for MPFR.".to_string(),
        ))
    })?;

    // Configuring
    log::info!("--- Configuring MPFR");
    let conf = std::process::Command::new("../configure")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg(&format!(
            "--prefix={}",
            &prefix
                .to_str()
                .expect("Unable to convert prefix path to str.")
        ))
        .arg(&format!(
            "--with-gmp={}",
            &prefix
                .to_str()
                .expect("Unable to convert prefix path to str.")
        ))
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing MPFR");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install Eigen
///
/// Contrary to what is advocated in `Caravel's` `INSTALL.md` file,
/// instead of downloading the release tarball, here we clone the
/// master branch of the repository. The reason for this approach
/// is that the name of the compressed eigen folder cannot be extracted
/// from the name of the tarball and would therefore require additional
/// input.
///
/// Once this is done, Eigen is configured and installed. Notice, that
/// this function also takes care of copying the `eigen3.pc` file from
/// the build directory to the install directory.
fn install_eigen(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("eigen.log");
    std::fs::File::create(&log_path)
        .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?;
    // Downloading the tarball
    log::info!("--- Cloning the repository");
    let conf = std::process::Command::new("git")
        .current_dir(&temp.join("src"))
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("clone")
        .arg(url)
        .arg("eigen")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Clone(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Clone(format!(
                "{}",
                e
            ))))
        }
    }
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join("eigen").join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for Eigen.".to_string(),
        ))
    })?;

    // Configuring
    log::info!("--- Configuring Eigen");
    let conf = std::process::Command::new("cmake")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("..")
        .arg(&format!(
            "-DCMAKE_INSTALL_PREFIX={}",
            &prefix
                .to_str()
                .expect("Unable to convert prefix path to str.")
        ))
        .arg(&format!(
            "-DCMAKE_INSTALL_DATADIR={}",
            std::path::Path::new("lib")
                .to_str()
                .expect("Unable to convert prefix path to str.")
        ))
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing Eigen");
    let conf = std::process::Command::new("make")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}

/// Install the Pentagon library.
///
/// The repository of our modified version of the Pentagon functions
/// is cloned, configured and then installed.
fn install_pentagons(
    url: &str,
    temp: &std::path::Path,
    prefix: &std::path::Path,
    n_cores: usize,
) -> Result<(), InstallError> {
    // Create the logfile for the terminal output of all invoked programs
    let log_path = temp.join("logs").join("pentagon.log");
    std::fs::File::create(&log_path)
        .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?;
    // Downloading the tarball
    log::info!("--- Cloning the repository");
    let conf = std::process::Command::new("git")
        .current_dir(&temp.join("src"))
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("clone")
        .arg(url)
        .arg("pentagons")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Clone(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Clone(format!(
                "{}",
                e
            ))))
        }
    }
    // Create build directory
    log::info!("--- Creating build directory");
    let build_dir = temp.join("src").join("pentagons").join("build");
    std::fs::create_dir(&build_dir).map_err(|_| {
        InstallError::new(InstallErrorStage::FileAccess(
            "Error creating build directory for the pentagon library.".to_string(),
        ))
    })?;

    // Configuring
    log::info!("--- Configuring Pentagon library");
    let conf = std::process::Command::new("meson")
        .current_dir(&build_dir)
        .env(
            "PKG_CONFIG_PATH",
            prefix
                .join("lib")
                .join("pkgconfig")
                .to_str()
                .expect("Unable to generate pkg-config prefix path from prefix"),
        )
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("..")
        .arg(&format!(
            "-Dprefix={}",
            &prefix
                .to_str()
                .expect("Unable to convert prefix path to str.")
        ))
        .arg("-Dbuildtype=release")
        .arg("-Dlibdir=lib")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Configuration(
                        format!("Got error code {}", code),
                    )));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Configuration(
                format!("{}", e),
            )))
        }
    }

    log::info!("--- Installing Pentagon library");
    let conf = std::process::Command::new("ninja")
        .current_dir(&build_dir)
        .stdout(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .stderr(
            std::fs::OpenOptions::new()
                .append(true)
                .open(&log_path)
                .map_err(|e| InstallError::new(InstallErrorStage::FileAccess(format!("{}", e))))?,
        )
        .arg("-j")
        .arg(&n_cores.to_string())
        .arg("install")
        .output();

    match conf {
        Ok(o) => {
            if !o.status.success() {
                if let Some(code) = o.status.code() {
                    return Err(InstallError::new(InstallErrorStage::Install(format!(
                        "Got error code {}",
                        code
                    ))));
                }
            }
        }
        Err(e) => {
            return Err(InstallError::new(InstallErrorStage::Install(format!(
                "{}",
                e
            ))))
        }
    }

    Ok(())
}
